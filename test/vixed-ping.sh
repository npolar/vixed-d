#!/usr/bin/env bash
set -e
set -o pipefail

# getopts option parsing inspired by https://stackoverflow.com/a/28466267
while getopts :rlo-: arg; do
    case $arg in
        - )  LONG_OPTARG="${OPTARG#*=}"
            case $OPTARG in
                request-file=?* )  REQUEST_FILE="$LONG_OPTARG" ;;
                log-file=?*     )  LOG_FILE="$LONG_OPTARG" ;;
                output-file=?*  )  OUTPUT_FILE="$LONG_OPTARG" ;;
                request-file*   )  echo "No arg for --$OPTARG option" >&2; exit 2 ;;
                log-file*   	   )  echo "No arg for --$OPTARG option" >&2; exit 2 ;;
                output-file*    )  echo "No arg for --$OPTARG option" >&2; exit 2 ;;
                '' )        break ;; # "--" terminates argument processing
                * )         echo "Illegal option --$OPTARG" >&2; exit 2 ;;
            esac ;;
        \? ) exit 2 ;;  # getopts already reported the illegal option
    esac
done
shift $((OPTIND-1)) # remove parsed options and args from $@ list

if [[ -z $REQUEST_FILE || -z $OUTPUT_FILE || -z $LOG_FILE ]]; then
    echo "One or more variables are undefined"
    exit 1
fi

if [[ -f $REQUEST_FILE ]]; then
    echo $(date) ": Starting processing" > $LOG_FILE
    echo $(date) ": ENV test: $HELLO $WORLD" >> $OUTPUT_FILE
    echo $(date) ": PONG!" >> $OUTPUT_FILE
    echo $(date) ": Executed processor $0 successfully" >> $LOG_FILE
else
    echo $(date) ": No REQUEST FILE found" >> $LOG_FILE
    exit 1
fi

