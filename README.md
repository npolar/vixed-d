# Visual Illustrator of eXternal Earth Data

Vixed is a system which produces localized illustrations of earth conditions
utilizing remote sensing data for operational and consultancy purposes

Vixed is a flexible multicomponent service that utilizes generic
communication protocols to deliver size optimized data to the users.


## Sample operational scenario
Below is a sequence diagram illustrating remote (ship) user interaction with `vixed`
through an email service.

```mermaid
sequenceDiagram

Captain->>MailServer: Send custom data request via email
MapMaker->>MailServer: Check for new requests
MapMaker->>SatelliteCloud: Check for new data
SatelliteCloud-->>MapMaker: Serve data
MapMaker->>MapMaker: Process data
MapMaker->>MailServer: Send map products as email
MailServer->>Captain: Forward the map products
```

# Dependencies

[MongoDB](https://www.mongodb.com/): Vixed-d uses MongoDB to store jobs and artifacts.

# Installation
## Binary installation

## Building from source / developing
* golang 1.8 or newer
* godog: https://github.com/DATA-DOG/godog.git
* mgo: https://github.com/globalsign/mgo
* email: https://github.com/scorredoira/email

Build `vixed-d` executable:

    CGO_ENABLED=0 go build -installsuffix cgo -ldflags '-s' vixed-d.go mailer.go
