Feature: Invoke external processors

  To allow processing of any kind of earth observation data
  The vixed daemon should be able to invoke external processors,
  handle processors exit codes and access produced output

  Generic processor is a dummy executable that implements the basic
  requirements in order to be executed by vixed-d.

  The minimum set of cli arguments should be at least:
    --input_request_file
    --output_file
    --log_file

  Scenario: Successfully invoke an external processor
    Given vixed-d is running
    And vixed-d finds an active job to execute external processor "foo"
    Then vixed-d invokes the processor with flags "input_request_file", "output_file", "log_file"
    When the exit state is "success"
    Then vixed-d returns the output product and logs
