package main

import (
	"bytes"
	"crypto/md5"
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"mime"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"

	mgo "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type Job struct {
	ID       string `json:"id"`
	Checksum string `json:"checksum"`
	Canceled bool   `json:"canceled"`

	// List of mail addresses that should receive the processing result
	Recipients []string `json:"send_to"`

	// Scheduling information
	Start    []time.Time `json:"start,omitempty"`
	Stop     time.Time   `json:"stop,omitempty"`
	Interval string      `json:"interval"`
	RunNow   bool        `json:"run_now"`

	// Processing information
	Proc     string          `json:"processor"`
	Settings json.RawMessage `json:"processor_settings"`
}

func NewJob() *Job {
	return &Job{ID: UUIDv4()}
}

// Validate checks the basic job parameters to see if it's a processable job.
// In case of errors an appropriate error will be returned.
func (j *Job) Valid() (valid bool, err []error) {
	// Handle stop time related errors
	if j.Stop.IsZero() {
		err = append(err, fmt.Errorf("No stop time provided"))
	}
	if j.Stop.Before(time.Now().UTC()) {
		err = append(err, fmt.Errorf("Stop time in the past"))
	}

	// Handle errors related to start time
	if len(j.Start) == 0 {
		err = append(err, fmt.Errorf("No start time provided"))
	} else {
		for _, st := range j.Start {
			if st.IsZero() {
				err = append(err, fmt.Errorf("Start time is zero"))
			}
			if st.After(j.Stop) {
				err = append(
					err, fmt.Errorf("Start time must come before stop"),
				)
			}
		}
	}

	// Handle interval format errors
	if _, perr := time.ParseDuration(j.Interval); perr != nil {
		err = append(err, fmt.Errorf("Invalid interval -> %s", perr))
	}

	valid = len(err) == 0

	return
}

// RunTime is used for executing jobs. Because jobs can have multiple starts
// they need to be split into indivdual runtimes in order to be scheduled
// correctly. It's based on the job object but exposes several methods to aid
// with the scheduling logic.
type RunTime struct {
	JobID string

	Interval time.Duration
	Start    time.Time
	Stop     time.Time
	LastRun  time.Time

	Proc        string
	Recipients  []string
	RequestFile string
}

// Started returns true if the runtime start is in the past
func (rt *RunTime) Started() bool {
	return time.Now().UTC().After(rt.Start)
}

// Finished returns true when: The runtime was started AND
// the stop time is in the past OR the next execution is in the past.
func (rt *RunTime) Finished() bool {
	return rt.Started() && (time.Now().UTC().After(rt.Stop) ||
		rt.NextRunTime().After(rt.Stop))
}

// SleepDuration returns the time duration we need to sleep the runtime for
// before executing the next job run.
func (rt *RunTime) SleepDuration() (sleep time.Duration) {
	if rt.Started() {
		sleep = time.Until(rt.LastRun.Add(rt.Interval))
	} else {
		sleep = time.Until(rt.Start)
	}
	return
}

// NextRuntime calculates the time of the next job run
func (rt *RunTime) NextRunTime() (next time.Time) {
	if rt.Started() {
		next = rt.LastRun.Add(rt.Interval).UTC()
	} else {
		next = rt.Start
	}
	return
}

// CalculateLastRun can be used to figure out when the theoretical last run
// was supposed to be. This is used when spinning up a jobs which were started
// in the past. It enables us to calculate the correct offsets for the next job
// run.
func (rt *RunTime) CalculateLastRun() {
	for rt.LastRun.Add(rt.Interval).Before(time.Now().UTC()) {
		rt.LastRun = rt.LastRun.Add(rt.Interval)
	}
}

// Exec start RunTime execution. It will start a go routine that waits until
// the next execution cycle. On execution it will call its self to schedule
// the next run.
func (rt *RunTime) Exec(run chan *RunTime) {
	// If LastRun hasn't been set yet we set it to the start time
	if rt.LastRun.IsZero() {
		rt.LastRun = rt.Start
	}

	go func() {
		if !rt.Finished() {
			// If the jobs start time has already passed and we've passed the
			// interval boundary we need to calculate the correct offset.
			if rt.Started() && time.Now().UTC().After(rt.NextRunTime()) {
				rt.CalculateLastRun()
			}

			// @TODO remove this log message since it's just for debug purposes
			log.Println(
				"Running Job: ",
				rt.JobID,
				"Next Run @",
				rt.NextRunTime().UTC().Format(time.RFC3339),
			)

			// Sleep unilt it's time to run the processing. When we leave sleep
			// we update the LastRun time to the current UTC time.
			time.Sleep(rt.SleepDuration())
			rt.LastRun = time.Now().UTC()
			rt.Exec(run) // Schedule the next run

			// Do work
			run <- rt
		}
	}()
}

type ProcConf struct {
	Executable string            `json:"exec"`
	EnvVars    map[string]string `json:"env"`
}

// Config defines the input configuration used by the JobHandler @ startup
type Config struct {
	// Data configuration
	FileDir string `json:"file_dir"`

	// Database configuration
	DBServers  []string `json:"servers"`
	Database   string   `json:"database"`
	Collection string   `json:"collection"`

	// HTTP configuration
	Host string `json:"host"`
	Port int    `json:"port"`

	// Mail configuration
	SMTPServer string   `json:"smtp"`
	SMTPPort   int      `json:"smtp_port"`
	IMAPServer string   `json:"imap"`
	IMAPPort   int      `json:"imap_port"`
	MailBox    string   `json:"mailbox"`
	MailUser   string   `json:"mail_user"`
	MailPass   string   `json:"mail_pass"`
	WhiteList  []string `json:"whitelist"`

	// Processor configuration
	Procs map[string]ProcConf `json:"processors"`
}

// DefaultConfig populates the config with some default values that should work
// when running in a local or test environment.
func DefaultConfig() *Config {
	return &Config{
		FileDir:    "/tmp/vixed",
		DBServers:  []string{"localhost:27017"},
		Database:   "vixed",
		Collection: "jobs",
		Host:       "localhost",
		Port:       8080,
		Procs:      make(map[string]ProcConf),
	}
}

type Processor struct {
	Name       string
	Receiver   chan *RunTime
	Executable string
	ENV        map[string]string
	Busy       bool
}

func (p *Processor) OutputName() string {
	mail_date := strings.Replace(
		time.Now().UTC().Format(time.RFC3339), ":", "_", -1,
	)
	return fmt.Sprintf("%s_%s", p.Name, mail_date)
}

func (p *Processor) LogName() string {
	mail_date := strings.Replace(
		time.Now().UTC().Format(time.RFC3339), ":", "_", -1,
	)

	return fmt.Sprintf("%s_%s.log", p.Name, mail_date)
}

type JobHandler struct {
	// Processor related information
	Procs   map[string]*Processor
	FileDir string

	// List of active runtimes
	RunTime map[string]*RunTime

	// Channels for communicating input and output
	Receiver chan *Job

	// SMTP mail support
	SMTP *MailSender

	// IMAP mail support
	IMAP *MailReceiver

	// Database collection we use to store job information. Connection is
	// based on configuration input and initialized when the Start() method
	// is invoked.
	DB *mgo.Collection

	// Srv is a http Server we use for showing job overviews, etc…
	Srv *http.Server
}

func NewJobHandler(cfg *Config) (jh *JobHandler) {
	// Use the provided configuration to connect to the database
	session, err := mgo.Dial(strings.Join(cfg.DBServers, ","))
	if err != nil {
		log.Fatal("Unable to connect to the database", err)
	}
	conf := session.DB(cfg.Database).C(cfg.Collection)

	// Configure the HTTP server
	server := &http.Server{
		Addr:              fmt.Sprintf("%s:%d", cfg.Host, cfg.Port),
		ReadTimeout:       15 * time.Second,
		WriteTimeout:      15 * time.Second,
		IdleTimeout:       15 * time.Second,
		ReadHeaderTimeout: 10 * time.Second,
		TLSConfig: &tls.Config{
			MinVersion: tls.VersionTLS12,
			CurvePreferences: []tls.CurveID{
				tls.CurveP256,
				tls.X25519,
			},
			PreferServerCipherSuites: true,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305, // Requires Go 1.8+
				tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,   // Requires Go 1.8+
				tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,
			},
		},
	}

	// Clean trailing slashes in the directory path and create the specified
	// directory if it doesn't yet exist
	if cfg.FileDir[len(cfg.FileDir)-1:] == "/" {
		cfg.FileDir = cfg.FileDir[:len(cfg.FileDir)-1]
	}
	err = os.MkdirAll(cfg.FileDir, 0700)
	if err != nil {
		log.Fatal("JobHandler - Failed to create file directory:", err)
	}

	// Setup the job handler
	jh = &JobHandler{
		Procs:    make(map[string]*Processor),
		RunTime:  make(map[string]*RunTime),
		Receiver: make(chan *Job, 10),
		FileDir:  cfg.FileDir,
		SMTP: NewMailSender(
			cfg.SMTPServer,
			cfg.MailUser,
			cfg.MailPass,
			cfg.SMTPPort,
		),
		IMAP: NewMailReceiver(
			cfg.IMAPServer,
			cfg.MailUser,
			cfg.MailPass,
			cfg.IMAPPort,
		),
		Srv: server,
		DB:  conf,
	}

	// Configure whitelist for IMAP message parsing.
	jh.IMAP.WhiteListAddresses(cfg.WhiteList)

	// Open the mailbox we want to read messages from.
	if err := jh.IMAP.Open(cfg.MailBox); err != nil {
		log.Fatal("Vixed-d - IMAP - Mailbox connection failed ->", err)
	}

	// Configure the router for our web server.
	mux := http.NewServeMux()
	mux.HandleFunc("/", jh.IndexHandler)
	mux.HandleFunc("/new", jh.NewJobHandler)
	jh.Srv.Handler = mux

	// Configure the Processors
	for name, settings := range cfg.Procs {
		jh.Procs[name] = &Processor{
			Name:       name,
			Receiver:   make(chan *RunTime),
			Executable: settings.Executable,
			ENV:        settings.EnvVars,
			Busy:       false,
		}
		jh.StartProc(name)
	}

	return
}

// IndexHandler returns a list of all running jobs.
func (jh *JobHandler) IndexHandler(w http.ResponseWriter, r *http.Request) {
	// Define an anonymous struct for our index response
	var rsp = struct {
		Active    []*Job `json:"active_jobs"`
		Scheduled []*Job `json:"scheduled_jobs"`
		Finished  []*Job `json:"finished_jobs"`
	}{}

	// Write the MongoDB queries for the different job states
	aq := bson.M{
		"start": bson.M{"$lt": time.Now().UTC()},
		"stop":  bson.M{"$gt": time.Now().UTC()},
	}
	sq := bson.M{"start": bson.M{"$gt": time.Now().UTC()}}
	fq := bson.M{"stop": bson.M{"$lt": time.Now().UTC()}}

	// Search for jobs
	jh.DB.Find(aq).All(&rsp.Active)
	jh.DB.Find(sq).All(&rsp.Scheduled)
	jh.DB.Find(fq).All(&rsp.Finished)

	// Create a json response
	rspJson, _ := json.MarshalIndent(rsp, "", "\t")
	w.WriteHeader(200)
	w.Header().Set("Content-Type", "application/json; charset=utf8")
	w.Write(rspJson)
}

func (jh *JobHandler) NewJobHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		// Create a buffer and read the request body
		var bodyBuf bytes.Buffer
		bodyBuf.ReadFrom(r.Body)
		r.Body.Close()

		// Create a new job and try to read the body contents into it
		j := NewJob()
		b := bodyBuf.Bytes()
		json.Unmarshal(b, j)
		j.Checksum = ChecksumJobJson(b)

		var procErr error
		if _, ok := jh.Procs[j.Proc]; !ok {
			procErr = fmt.Errorf("Unknown processor: %s", j.Proc)
		}

		exists := jh.ExistingJob(j.Checksum)

		if valid, errs := j.Valid(); valid && !exists {
			// Push the job to the receiver channel
			jh.Receiver <- j

			w.WriteHeader(http.StatusCreated)
			w.Write([]byte("[Vixed-d] - NEW - Accepted job with ID: " + j.ID))
		} else {
			if exists {
				errs = append(errs, fmt.Errorf("Job already exists"))
			}
			if procErr != nil {
				errs = append(errs, procErr)
			}
			w.WriteHeader(http.StatusUnprocessableEntity)
			w.Write([]byte(
				"[Vixed-d] - NEW - Invalid job request.\r\n\r\nErrors:\r\n",
			))
			for i, err := range errs {
				fmt.Fprintf(w, "%d - %s\r\n", i+1, err)
			}
		}
	} else {
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte("[Vixed-d] - NEW - Unsupported method: " + r.Method))
	}
}

// txtRxp is used to check for any text/<type> mime-types.
var txtRxp = regexp.MustCompile(`^text\/.*$`)

// StartProc starts an invokation loop for the named processor. It sits and
// waits for data on the channel. After receiving data the given processor is
// invoked. The processor will only handle one request at a time. If new
// requests hit the receiver they'll have to wait unilt completion before
// they will need to wait for the previous job to finish.
func (jh *JobHandler) StartProc(name string) {
	p := jh.Procs[name]
	go func() {
		for {
			r := <-p.Receiver
			log.Println("Hello from", p.Name, "Running:", p.Executable)

			outputName := fmt.Sprintf("%s/%s", jh.FileDir, p.OutputName())
			logName := fmt.Sprintf("%s/%s", jh.FileDir, p.LogName())

			// Setup the command
			cmd := exec.Command(
				p.Executable,
				fmt.Sprintf("--request-file=%s", r.RequestFile),
				fmt.Sprintf("--output-file=%s", outputName),
				fmt.Sprintf("--log-file=%s", logName),
			)

			// Set the processor environment
			cmd.Env = os.Environ()
			for k, v := range p.ENV {
				cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", k, v))
			}

			// Run processor on the Linux terminal
			out, err := cmd.CombinedOutput()
			if err != nil {
				log.Println("Job Processing Failed:", out, err)
				jh.SMTP.ProcessError(r.Recipients, r.JobID, string(out), err)
			} else {
				// Try to figure out the type and extension for the file. If
				// found rename the output to include the extension.
				mType, ext := DetectFileType(outputName)
				if ext != "" {
					err = os.Rename(outputName, outputName+ext)
					if err != nil {
						log.Println(err)
					} else {
						outputName = outputName + ext
					}
				}

				// If mime type is text we'll send the result in the mail body
				// by using the jh.SMTP.TextResult function.
				if txtRxp.MatchString(mType) {
					o, _ := ioutil.ReadFile(outputName)
					log.Printf("Output: \n%s\n-------\n", o)
					jh.SMTP.TextResult(
						r.Recipients,
						r.JobID,
						outputName,
						logName,
					)
				} else {
					jh.SMTP.Result(r.Recipients, r.JobID, outputName, logName)
				}
				l, _ := ioutil.ReadFile(logName)
				log.Printf("Log: %s", l)

				// Clean up artifacts
				os.Remove(outputName)
				os.Remove(logName)
			}
		}
	}()
}

// StartScheduler spawns a new go routine that  waits for new jobs to land on
// the receiver channel. When a job is detected it will validate and store the
// job request. After that it will spawn runtimes as needed. If any errors are
// detected they are returned on the correct response channel.
func (jh *JobHandler) StartScheduler() {
	go func() {
		for {
			// Wait until we receive a new job on the channel
			j := <-jh.Receiver
			// If RunNow key is True, create Start and Stop times on the fly
			if j.RunNow {
				timestamp := time.Now().UTC()
				j.Start = []time.Time{timestamp.Add(time.Second * 5)}
				j.Stop = timestamp.Add(time.Second * 10)
				j.Checksum = fmt.Sprintf("%x", md5.Sum([]byte(timestamp.Format(time.RFC3339))))
			}

			valid, errs := j.Valid()
			exists := jh.ExistingJob(j.Checksum)

			if exists {
				errs = append(errs, fmt.Errorf("Job already exists"))
			}

			if valid && !exists {
				// Check if the processor exists
				if _, ok := jh.Procs[j.Proc]; ok {
					// Store the job in the database
					jh.DB.Insert(j)

					// Setup a RunTime for the job
					jh.SetupRunTime(j)

					// Send an acceptance message to the user
					// unless it's one time forecast request
					if j.Proc != "yr-forecast" {
						go jh.SMTP.Accepted(j.Recipients, j.ID)
					}
				} else {
					errs = append(
						errs,
						fmt.Errorf(
							"Vixed-D - Job - Unknown processor: %s",
							j.Proc,
						),
					)
				}
			}

			// If any errors are detected send a rejection mail
			if len(errs) > 0 {
				go jh.SMTP.Rejected(j.Recipients, j.ID, errs)
			}
		}
	}()
}

// ExistingJob queries the database for items with a matching checksum.
// if something is returned the job already exist.
func (jh *JobHandler) ExistingJob(checksum string) bool {
	var r []interface{}
	q := bson.M{"checksum": bson.M{"$eq": checksum}}
	jh.DB.Find(q).All(&r)
	return len(r) > 0
}

// StartMailReceiver starts listening for new jobs on the configured mailbox
func (jh *JobHandler) StartMailReceveiver() {
	// Start listening for new jobs
	jh.IMAP.Listen(jh.Receiver)
}

// SetupRunTime creates new RunTime objects based on the start dates in the
// provided job object. After RunTime initialization it calls RunTime.Exec()
// on it.
func (jh *JobHandler) SetupRunTime(j *Job) {
	d, _ := time.ParseDuration(j.Interval)

	// Create a request file for the job
	requestFile := fmt.Sprintf("%s/%s-request.json", jh.FileDir, j.ID)
	err := ioutil.WriteFile(requestFile, j.Settings, 0600)
	if err != nil {
		log.Println("RunTime - Setup - Error creating request file:", err)
	}

	// After we receive a job we create a runtime for each start time
	for _, s := range j.Start {
		var rt = RunTime{
			JobID:       j.ID,
			Interval:    d,
			Start:       s,
			Stop:        j.Stop,
			Recipients:  j.Recipients,
			RequestFile: requestFile,
		}

		// use the md5sum of the json representation of the runtime as the
		// runtime ID. After storing the runtime reference start it.
		b, _ := json.Marshal(rt)
		jh.RunTime[fmt.Sprintf("%x", md5.Sum(b))] = &rt
		rt.Exec(jh.Procs[j.Proc].Receiver)
	}
}

// LoadExsistingJobs will look in the database for active jobs and will set up
// runtimes for them.
func (jh *JobHandler) LoadExistingJobs() {
	var jobs []*Job
	// A query that looks for all jobs that haven't finished execution yet.
	q := bson.M{"stop": bson.M{"$gt": time.Now().UTC()}}
	jh.DB.Find(q).All(&jobs)

	// Loop through the resulting list of jobs and setup the runtimes.
	for _, j := range jobs {
		jh.SetupRunTime(j)
	}
}

// Start bootstraps all the main JobHandler components so it can receive jobs
// process them and respond back the result to the person who issued the job.
func (jh *JobHandler) Start() {
	jh.LoadExistingJobs()
	jh.StartScheduler()
	jh.StartMailReceveiver()

	// Start the web server. This is also used as a blocker to prevent the
	// program from exiting.
	if err := jh.Srv.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}

// ChecsumJson reads the job json contents into a map and generates checksums
// for them to determine if the job already exists or not.
func ChecksumJobJson(body []byte) string {
	var controlStruct = make(map[string]interface{})
	json.Unmarshal(body, &controlStruct)
	sortedByMap, _ := json.Marshal(controlStruct)
	return fmt.Sprintf("%x", md5.Sum(sortedByMap))
}

// DetectFileType uses the unix file utility to detect the mime-type of the
// output file. This is then used together with the mime package to figure
// out valid file extensions. It returns the mime type and the first associated
// extension to the caller.
func DetectFileType(file string) (mType, ext string) {
	t, err := exec.Command("file", "--mime-type", file).Output()
	if err != nil {
		log.Println("Couldn't determine file type", err)
	} else {
		mType = strings.Split(string(t), ": ")[1]
		// strip newline
		mType = strings.Trim(mType, "\r\n")
		exts, err := mime.ExtensionsByType(mType)
		if err == nil {
			ext = exts[0]
		} else {
			log.Println("Error detecting file extension", err)
		}
	}
	return mType, ext
}

// UUIDv4 uses the /dev/urandom file to get a random character sequence and
// formats it as a version 4 (random) UUID.
func UUIDv4() string {
	f, err := os.Open("/dev/urandom")
	if err != nil {
		log.Fatal("Error generating random uuid:", err)
	}
	defer f.Close()

	b := make([]byte, 16)
	f.Read(b)
	// Set the version bit to indicate UUIDv4
	b[6] = (b[6] & 0x0f) | (4 << 4)
	return fmt.Sprintf("%x-%x-%x-%x-%x", b[:4], b[4:6], b[6:8], b[8:10], b[10:])
}

func main() {
	// Setup a command line flag to capture the Config path
	var config string
	flag.StringVar(&config, "c", "config.json", "Configuration file")
	flag.Parse()

	// Load the configuration
	f, err := os.Open(config)
	if err != nil {
		log.Fatal("Error opening the configuration file", err)
	}

	// Create a buffer and read the config file
	var buf bytes.Buffer
	buf.ReadFrom(f)
	f.Close() // close the file after we're done

	// Unmarshal the configuration json into the Config object.
	var cfg *Config = DefaultConfig()
	json.Unmarshal(buf.Bytes(), &cfg)

	// Initialize a new JobHandler with the given Config and start it
	NewJobHandler(cfg).Start()
}
