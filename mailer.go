package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime"
	"mime/multipart"
	"net/mail"
	"net/smtp"
	"regexp"
	"strconv"
	"strings"
	"time"

	imap "github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/scorredoira/email"
)

type MailSender struct {
	server string
	from   mail.Address
	auth   smtp.Auth
}

func NewMailSender(server, user, pwd string, port int) *MailSender {
	return &MailSender{
		server: fmt.Sprintf("%s:%d", server, port),
		from: mail.Address{
			Name:    "Vixed-d Mailer",
			Address: user,
		},
		auth: smtp.PlainAuth("", user, pwd, server),
	}
}

// Accepted is used to send a mail to the configured recipients once a new job
// is accepted into the system.
func (s *MailSender) Accepted(to []string, jobID string) {
	msg := email.NewMessage(
		fmt.Sprintf("Vixed-d - Accepted new job: %s", jobID),
		"A new job has been installed with ID: "+jobID,
	)
	msg.From = s.from
	msg.To = to

	if err := email.Send(s.server, s.auth, msg); err != nil {
		log.Println("Vixed-d - Mailer - Error sending acceptance message:", err)
	}
}

// Rejected is sent to inform the user of invalid job parameters
func (s *MailSender) Rejected(to []string, jobID string, errs []error) {
	// Create a body containing the validation errors that occured.
	var body string = fmt.Sprintf(
		"Job: %s was rejected with errors:\r\n\r\n",
		jobID,
	)
	for i, err := range errs {
		body += fmt.Sprintf("%d - %s\r\n", i, err)
	}

	msg := email.NewMessage(
		fmt.Sprintf("Vixed-d - Job: %s exited with errors", jobID),
		body,
	)

	msg.From = s.from
	msg.To = to

	if err := email.Send(s.server, s.auth, msg); err != nil {
		log.Println("Vixed-d - Mailer - Error sending error message:", err)
	}
}

// ProcessError is sent to inform the user of errors during process invocation
func (s *MailSender) ProcessError(to []string, jobID, out string, err error) {
	// Create a body containing the errors that occured.
	var body string = fmt.Sprintf(
		"Job: %s - Processor exited with errors:\r\n\r\n",
		jobID,
	)

	body += fmt.Sprintf("PROCESSOR LOG:\r\n\r\n%s\r\n", out)
	body += fmt.Sprintf("--------------")
	body += fmt.Sprintf("EXIT CODE:\r\n\r\n%s\r\n", err)

	msg := email.NewMessage(
		fmt.Sprintf("Vixed-d - Job: %s exited with errors", jobID),
		body,
	)

	msg.From = s.from
	msg.To = to

	if err := email.Send(s.server, s.auth, msg); err != nil {
		log.Println("Vixed-d - Mailer - Error sending error message:", err)
	}
}

func (s *MailSender) Result(to []string, jobID, result, logFile string) {
	msg := email.NewMessage(
		fmt.Sprintf("Vixed-d - Processing output for job: "+jobID),
		"Check attachments for results. Log output below:\r\n\r\n",
	)
	msg.From = s.from
	msg.To = to

	if err := msg.Attach(result); err != nil {
		log.Println("Vixed-d - Mailer - Error attaching result file:", err)
		return
	}

	if data, err := ioutil.ReadFile(logFile); err != nil {
		log.Println("Vixed-d - Mailer - Error reading log file:", err)
		return
	} else {
		msg.Body += fmt.Sprintf("LOG:\r\n\r\n%s", string(data))
	}

	if err := email.Send(s.server, s.auth, msg); err != nil {
		log.Println("Vixed-d - Mailer - Error sending result mail:", err)
	}
}

func (s *MailSender) TextResult(to []string, jobID, result, logFile string) {
	msg := email.NewMessage(
		fmt.Sprintf("Vixed-d - Processing output for job: "+jobID),
		"Results and log output below:\r\n\r\n",
	)
	msg.From = s.from
	msg.To = to

	if data, err := ioutil.ReadFile(result); err != nil {
		log.Println("Vixed-d - Mailer - Error attaching result file:", err)
		return
	} else {
		msg.Body += fmt.Sprintf("OUTPUT:\r\n\r\n%s\r\n\r\n", string(data))
	}

	if data, err := ioutil.ReadFile(logFile); err != nil {
		log.Println("Vixed-d - Mailer - Error reading log file:", err)
		return
	} else {
		msg.Body += fmt.Sprintf("LOG:\r\n\r\n%s", string(data))
	}

	if err := email.Send(s.server, s.auth, msg); err != nil {
		log.Println("Vixed-d - Mailer - Error sending result mail:", err)
	}
}

type MailReceiver struct {
	whiteList map[string]bool
	pollTime  time.Duration

	*client.Client
}

func NewMailReceiver(server, user, pass string, port int) *MailReceiver {
	c, err := client.DialTLS(fmt.Sprintf("%s:%d", server, port), nil)
	if err != nil {
		log.Println("Vixed - Mailer - IMAP failed to connect:", err)
	}

	err = c.Login(user, pass)
	if err != nil {
		log.Println("Vixed-d - Mailer - IMAP failed to login:", err)
	}

	return &MailReceiver{
		whiteList: make(map[string]bool),
		pollTime:  15 * time.Second,
		Client:    c,
	}
}

var mailFormatRegexp = regexp.MustCompile(`^.*@.*\..{2,3}$`)

func (mr *MailReceiver) WhiteListAddresses(addresses []string) {
	for _, addr := range addresses {
		if mailFormatRegexp.MatchString(addr) {
			mr.whiteList[strings.ToLower(addr)] = true
		} else {
			log.Println(
				"Vixed-d - Mailer - Mallformed whitelist address. Skipping ->",
				addr,
			)
		}
	}
}

func (mr *MailReceiver) Open(mailbox string) (err error) {
	_, err = mr.Select(mailbox, false)
	if err == nil {
		log.Println("Total Messages:", mr.Mailbox().Messages)
	}
	return err
}

// DeleteMail allows us to delete a set of messages in the mailbox.
func (mr *MailReceiver) DeleteMail(seq *imap.SeqSet) {
	// First mark the message as deleted
	item := imap.FormatFlagsOp(imap.AddFlags, true)
	flags := []interface{}{imap.DeletedFlag}
	if err := mr.Store(seq, item, flags, nil); err != nil {
		log.Fatal(err)
	}

	// Then delete it
	if err := mr.Expunge(nil); err != nil {
		log.Fatal(err)
	}
}

// Listen starts a job that polls the configured mail box for new messages
// according to the specified poller interval. If a message is detected that
// fills the requirements it will generate a new job entry on the channel
// provided as the function argument.
func (mr *MailReceiver) Listen(job chan *Job) {
	go func() {
		for {
			mbox := mr.Mailbox()

			// If new messages are received we'll handle them
			if mbox.Messages > 0 {
				section := imap.BodySectionName{}
				seqSet := imap.SeqSet{}
				seqSet.AddRange(uint32(1), mbox.Messages)
				items := []imap.FetchItem{section.FetchItem()}

				mails := make(chan *imap.Message, 10)

				go func() {
					mr.Fetch(&seqSet, items, mails)
				}()

				// Loop over all the new messages and check for jobs.
				for msg := range mails {
					content := msg.GetBody(&section)
					if content.Len() == 0 {
						log.Println(
							"Vixed-d - Mailer - Content lenght is 0",
						)
						break
					}

					m, err := mail.ReadMessage(content)
					if err != nil {
						log.Println(
							"Vixed-d - Mailer - Failed to read mail content ->",
							err,
						)
						break
					}

					// Check if the sender of the message is on the whitelist
					sender, err := mail.ParseAddress(m.Header.Get("From"))
					if err != nil {
						log.Println(
							"Vixed-d - Mailer - Failed to parse sender",
							err,
						)
						break
					}

					sa := strings.ToLower(sender.Address)
					if _, ok := mr.whiteList[sa]; !ok {
						log.Println(
							"Vixed-d - Mailer - Sender not on whitelist",
							sa,
						)
						break
					}

					// Check what kind of message we are dealing with. We
					// are looking of mixed/multipart content.
					mType, params, err := mime.ParseMediaType(
						m.Header.Get("Content-Type"),
					)
					if err != nil {
						log.Println(
							"Vixed-d - Mailer - Failed to parse content-type",
							err,
						)
						break
					}

					// when request is provided inline
					// email body
					if mType == "text/plain" {
						HandleTextPlainMessage(
							m.Body,
							job,
							sender.Address,
						)
						break
					}

					if mType == "multipart/mixed" ||
						mType == "multipart/alternative" {
						err := ReadMultipartMessage(
							m.Body,
							params["boundary"],
							sender.Address,
							job,
						)
						if err != nil {
							log.Println(
								"Vixed-d - Mailer - Error processing mail ->",
								err,
							)
						}
					}
				}

				// Delete all the messages read and reset the message state
				mr.DeleteMail(&seqSet)
			}
			// Check the mailbox according to the configured poll interval
			time.Sleep(mr.pollTime)

			if err := mr.Check(); err != nil {
				log.Println("Vixed-d - Mailer - Check mailbox:", err)
				log.Fatal("Vixed-d - Mailer - Logging out! ->", mr.Logout())
				return
			}
		}
	}()
}

var bodyCoordinatesRegexp = regexp.MustCompile(
	`(?m)[Ll]at:\s*(\-?\d+(\.\d+?)?)?\s*[;,\n]\s*[Ll]on:\s*(\-?\d+(\.\d+_?)?)?`,
)

func ParseBodyForCoordinates(body string) (lat, lon float64) {
	result := bodyCoordinatesRegexp.FindStringSubmatch(
		strings.Replace(body, "&nbsp;", "", -1),
	)
	if len(result) == 5 {
		lat, _ = strconv.ParseFloat(result[1], 64)
		lon, _ = strconv.ParseFloat(result[3], 64)
	}
	return lat, lon
}

func HandleTextPlainMessage(body io.Reader, j chan *Job, sender string) {
	// looking for the body part that contains text
	var pbuf bytes.Buffer
	pbuf.ReadFrom(body)
	lat, lon := ParseBodyForCoordinates(pbuf.String())
	textJob := NewJob()
	textJob.Start = []time.Time{
		time.Now().UTC().Add(time.Second * 5),
	}
	textJob.Stop = textJob.Start[0]
	textJob.Interval = "10s"
	textJob.Proc = "yr-forecast"
	textJob.Recipients = []string{
		sender,
	}
	var RequestSettings = struct {
		Lat float64 `json:"lat"`
		Lon float64 `json:"lon"`
	}{lat, lon}

	reqJson, _ := json.MarshalIndent(RequestSettings, "", "\t")
	textJob.Settings = reqJson

	data, _ := json.Marshal(textJob)
	textJob.Checksum = ChecksumJobJson(data)
	j <- textJob
}

// requestPattern is the regular expression used for detecting job requests.
// It expects to find an attachment with filename <something>.json.
var requestPattern = regexp.MustCompile(`.*\.json`)

// ReadMultipartMessage takes an io.Reader containing the message body, the
// multipart boundary and a channel on which to publish jobs that are discovered
// within the message.
func ReadMultipartMessage(body io.Reader, b, sender string, j chan *Job) (err error) {
	bodyReader := multipart.NewReader(body, b)

	for err == nil {
		var part *multipart.Part
		part, err = bodyReader.NextPart()

		// If body part is HTML try to capture coordinates for the
		// forecast delivery processor
		// TODO: decouple particular processor from text parsing bit
		if err == nil {
			mType, _, _ := mime.ParseMediaType(part.Header.Get("Content-Type"))
			if mType == "text/html" {
				HandleTextPlainMessage(part, j, sender)
			}
		}

		// If no errors are detected while reading the next part and the parts
		// filename matches our pattern we'll attempt to read the job contents
		// and dispatch them to the job channel for scheduling by the job
		// handler.
		if err == nil && requestPattern.MatchString(part.FileName()) {
			var data []byte
			var buf bytes.Buffer
			buf.ReadFrom(part)

			data, err = base64.StdEncoding.DecodeString(buf.String())
			if err == nil {
				mailJob := NewJob()
				if err = json.Unmarshal(data, mailJob); err == nil {
					mailJob.Checksum = ChecksumJobJson(data)
					j <- mailJob
				}
			}
		}
	}

	if err == io.EOF {
		err = nil
	}

	return
}
